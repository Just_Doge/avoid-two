//
//  GameScene.swift
//  Avo!2
//
//  Created by Geddy on 21/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var player = SKSpriteNode()
var spike = SKSpriteNode()
var ground = SKSpriteNode()

var lblMain = SKLabelNode()
var lblScore = SKLabelNode()

var playerSize = CGSize(width: 60, height: 60)
var spikeSize = CGSize(width: 17, height: 130)

var spikeSpeed = 2.1
var isAlive = true

var score = 0

var spikeSpawnTime = 0.2

struct PhysicsCategory {
    static let player: UInt32 = 1
    static let spike: UInt32 = 2
}

var touchLocation = CGPoint()

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    
    override func didMove(to view: SKView) {
        self.backgroundColor = orangeColor
        physicsWorld.contactDelegate = self
        
        resetGame()
        spawnLblMain()
        spawnLblScore()
        spawnGround()
        spawnPlayer()
        
        timerSpawnSpikesLeft()
        timerSpawnSpikesRight()
        timerScore()
    }
    
    func resetGame() {
        isAlive = true
        score = 0
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            touchLocation = touch.location(in: self)
            movePlayerOnTouch()
        }
    }
    
    func movePlayerOnTouch() {
        if isAlive == true {
            player.position.x = touchLocation.x
        }
    }
    
    func spawnPlayer() {
        player = SKSpriteNode(imageNamed: "techCircle")
        player.size = playerSize
        player.position = CGPoint(x: self.frame.midX, y: ground.position.y + 170)
        
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.categoryBitMask = PhysicsCategory.player
        player.physicsBody?.categoryBitMask = PhysicsCategory.player
        player.physicsBody?.contactTestBitMask = PhysicsCategory.spike
        player.physicsBody?.isDynamic = false
        player.name = "playerName"
        player.zPosition = 1
        
        let rotate = SKAction.rotate(byAngle: 14, duration: 1.0)
        player.run(SKAction.repeatForever(rotate))
        self.addChild(player)
    }
    
    func spawnSpikeRight() {
        spike = SKSpriteNode(imageNamed: "spike")
        spike.size = spikeSize
        
        spike.position.x =  CGFloat(arc4random_uniform(UInt32(self.frame.maxX)))
        spike.position.y = self.frame.height + 190
        
        spike.physicsBody = SKPhysicsBody(rectangleOf: spike.size)
        spike.physicsBody?.affectedByGravity = false
        spike.physicsBody?.allowsRotation = false
        spike.physicsBody?.categoryBitMask = PhysicsCategory.spike
        spike.physicsBody?.contactTestBitMask = PhysicsCategory.player
        spike.physicsBody?.isDynamic = true
        spike.name = "spikeName"
        moveSpikeToGround()
        
        self.addChild(spike)
        
    }
    
    func spawnSpikeLeft() {
        spike = SKSpriteNode(imageNamed: "spike")
        spike.size = spikeSize
        
        spike.position.x = -1 * CGFloat(arc4random_uniform(UInt32(self.frame.maxX)))
        spike.position.y = self.frame.height + 190
        
        spike.physicsBody = SKPhysicsBody(rectangleOf: spike.size)
        spike.physicsBody?.affectedByGravity = false
        spike.physicsBody?.allowsRotation = false
        spike.physicsBody?.categoryBitMask = PhysicsCategory.spike
        spike.physicsBody?.contactTestBitMask = PhysicsCategory.player
        spike.physicsBody?.isDynamic = true
        spike.name = "spikeName"
        moveSpikeToGround()
        
        self.addChild(spike)
        
    }
    
    func moveSpikeToGround() {
        let moveToY = SKAction.moveTo(y: ground.position.y + 150, duration: spikeSpeed)
        let fadeOut = SKAction.fadeOut(withDuration: 0.1)
        let destroy = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([moveToY, fadeOut, destroy])
        spike.run(sequence)
        
    }
    
    func spawnGround() {
        ground = SKSpriteNode(color: offBlackColor, size: CGSize(width: self.frame.width, height: 300))
        ground.position = CGPoint(x: self.frame.midX, y: self.frame.minY)
        
        self.addChild(ground)
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontColor = offWhiteColor
        lblMain.fontSize = 100
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 375)
        lblMain.text = "Start!"
        
        self.addChild(lblMain)
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontColor = offWhiteColor
        lblScore.fontSize = 30
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 375)
        lblScore.text = "You Have been going for: \(score) seconds!"
        
        self.addChild(lblScore)
    }
    
    func timerSpawnSpikesLeft() {
        let wait = SKAction.wait(forDuration: spikeSpawnTime * 3)
        let spawn = SKAction.run {
            if isAlive == true {
                self.spawnSpikeLeft()
                
                
            }
        }
        
        let sequence = SKAction.sequence([wait, spawn])
        self.run(SKAction.repeatForever(sequence))
    }
    
    func timerSpawnSpikesRight() {
        let wait = SKAction.wait(forDuration: spikeSpawnTime)
        let spawn = SKAction.run {
            if isAlive == true {
                self.spawnSpikeRight()
                
                
            }
        }
        
        let sequence = SKAction.sequence([wait, spawn])
        self.run(SKAction.repeatForever(sequence))
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        
        if ((firstBody.categoryBitMask == PhysicsCategory.player) && (secondBody.categoryBitMask == PhysicsCategory.spike) || (firstBody.categoryBitMask == PhysicsCategory.spike) && (secondBody.categoryBitMask == PhysicsCategory.player)) {
            spikePlayerCollision(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
        }
    }
    
    func spikePlayerCollision(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        if contactA.name == "playerName" && contactB.name == "spikeName" || contactA.name == "spikeName" && contactB.name == "playerName" {
            isAlive = false
            gameOverLogic()
            player.alpha = 0.0
        }
    }
    func gameOverLogic() {
        lblMain.fontSize = 45
        lblMain.text = "Your Crazy Tire Popped!"
        
        moveToTitleScene()
    }
    func updateScore() {
        lblScore.text = "You Have been going for: \(score) seconds!"
        if score >= 800 {
            lblScore.fontSize = 20
        }
    }
    func moveToTitleScene() {
        let wait = SKAction.wait(forDuration: 3.0)
        let changeScene = SKAction.run {
            self.view?.presentScene(TitleScene(), transition: .doorway(withDuration: 0.8))
        }
        
        let sequence = SKAction.sequence([wait, changeScene])
        self.run(SKAction.repeat(sequence, count: 1))
        
        if let theScene = TitleScene(fileNamed: "TitleScene") {
            let skView = self.view as! SKView
            skView.ignoresSiblingOrder = true
            
            theScene.scaleMode = .aspectFill
            
            skView.presentScene(theScene, transition: .doorway(withDuration: 0.8))
        }
        
    }
    
    func timerScore() {
        let wait = SKAction.wait(forDuration: 1.0)
        let scoreAdd = SKAction.run {
            if isAlive {
                score += 1
                self.updateScore()
            }
        }
        
        let sequenceForever = SKAction.repeatForever(SKAction.sequence([wait, scoreAdd]))
        self.run(sequenceForever)
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
