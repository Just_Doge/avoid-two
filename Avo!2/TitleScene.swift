//
//  TitleScene.swift
//  Avo!2
//
//  Created by Geddy on 21/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import Foundation
import SpriteKit


var topSprite = SKSpriteNode()
var bottomSprite = SKSpriteNode()

var orangeColor = UIColor.orange
var offBlackColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
var gameTitle = SKLabelNode()
var btnPlay: UIButton!

var mainSprite = SKSpriteNode()

class TitleScene: SKScene {
    
    
    override func didMove(to view: SKView) {
        self.backgroundColor = offBlackColor
        spawnTopSprite()
        spawnBottomSprite()
        setUpText()
        spawnMainSprite()
    }
    
    func spawnMainSprite() {
        mainSprite = SKSpriteNode(imageNamed: "techCircle")
        mainSprite.size = CGSize(width: 400, height: 400)
        mainSprite.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        let rotate = SKAction.rotate(byAngle: 2, duration: 1.0)
        mainSprite.run(SKAction.repeatForever(rotate))
        
        self.addChild(mainSprite)
    }
    
    func spawnTopSprite() {
        let tempSize = CGSize(width: self.frame.width * 2, height: 300)
        
        topSprite = SKSpriteNode(color: orangeColor, size: tempSize)
        topSprite.color = orangeColor
        topSprite.position = CGPoint(x: self.frame.midX, y: self.frame.maxY - 100)
        topSprite.zRotation = 0.1
        
        self.addChild(topSprite)
    }
    
    func spawnBottomSprite() {
        let tempSize = CGSize(width: self.frame.width * 2, height: 390)
        
        bottomSprite = SKSpriteNode(color: orangeColor, size: tempSize)
        bottomSprite.color = orangeColor
        bottomSprite.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 60)
        bottomSprite.zRotation = 0.13
        
        self.addChild(bottomSprite)
    }
    func setUpText() {
        gameTitle = SKLabelNode(fontNamed: "Futura")
        gameTitle.fontSize = 195
        gameTitle.fontColor = offWhiteColor
        gameTitle.position = CGPoint(x: self.frame.midX, y: self.frame.height - 900)
        gameTitle.text = "Avo!2"
        gameTitle.zRotation = 0.1
        self.addChild(gameTitle)
        
        btnPlay = UIButton(frame: CGRect(x: 100, y: 100, width: self.frame.width, height: 200))
        btnPlay.center = CGPoint(x: view!.frame.size.width / 2, y: self.frame.midY + 530)
        btnPlay.titleLabel?.font = UIFont(name: "Futura", size: self.frame.width / 8)
        btnPlay.setTitle("Play", for: .normal)
        btnPlay.setTitleColor(offWhiteColor, for: .normal)
        btnPlay.addTarget(self, action: #selector(playTheGame), for: .touchUpInside)
        
        
        self.view?.addSubview(btnPlay)
    }
    
    func playTheGame() {
        let theTransition = SKTransition.doorway(withDuration: 0.6)
        self.view?.presentScene(GameScene(), transition: theTransition)
        btnPlay.removeFromSuperview()
        gameTitle.removeFromParent()
        topSprite.removeFromParent()
        bottomSprite.removeFromParent()
        mainSprite.removeFromParent()
        
        if let theScene = GameScene(fileNamed: "GameScene") {
            let skView = self.view as! SKView
            skView.ignoresSiblingOrder = true
            
            theScene.scaleMode = .aspectFill
            
            skView.presentScene(theScene, transition: theTransition)
        }
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
}
